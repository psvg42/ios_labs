//
//  ViewController.h
//  Calc4
//
//  Created by admin on 12.10.14.
//  Copyright (c) 2014 Troino&Sypityy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong) UITextField *input;
@property (assign) int val1;
@property (assign) int lastOperation;

@end

