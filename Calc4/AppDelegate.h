//
//  AppDelegate.h
//  Calc4
//
//  Created by admin on 12.10.14.
//  Copyright (c) 2014 Troino&Sypityy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

