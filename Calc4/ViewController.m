//
//  ViewController.m
//  Calc4
//
//  Created by admin on 12.10.14.
//  Copyright (c) 2014 Troino&Sypityy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


- (void)viewDidLoad {
    self.input = [[UITextField alloc] initWithFrame:CGRectMake(20, 50, 200, 60)];
    self.input.borderStyle = UITextBorderStyleRoundedRect;
    self.input.layer.cornerRadius = 10;
    self.input.clipsToBounds = YES;
    self.input.layer.borderColor = [[UIColor blackColor]CGColor];
    self.input.enabled = NO;
    self.input.text = @"0";
    UIImageView *bg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 375, 667)];
    bg.image = [UIImage imageNamed:@"Hello-Kitty.jpg"];
    bg.alpha = 0.25;
    [self.view addSubview:bg];
    [self.view addSubview:self.input];
    [super viewDidLoad];
    NSLog(@"%f", [UIScreen mainScreen].bounds.size.width);
    NSLog(@"%f", [UIScreen mainScreen].bounds.size.height);

    [self addButtonWithText:@"0" x:20 y:370];
    [self addButtonWithText:@"1" x:20 y:290];
    [self addButtonWithText:@"2" x:90 y:290];
    [self addButtonWithText:@"3" x:160 y:290];
    [self addButtonWithText:@"4" x:20 y:210];
    [self addButtonWithText:@"5" x:90 y:210];
    [self addButtonWithText:@"6" x:160 y:210];
    [self addButtonWithText:@"7" x:20 y:130];
    [self addButtonWithText:@"8" x:90 y:130];
    [self addButtonWithText:@"9" x:160 y:130];
    
    [self addButtonWithText:@"+" x:230 y:290 width:60 height:140  sel:@selector(plus)];
    [self addButtonWithText:@"-" x:300 y:290 sel:@selector(minus)];
    [self addButtonWithText:@"=" x:300 y:370 sel:@selector(equal)];
    [self addButtonWithText:@"*" x:300 y:130 sel:@selector(mult)];
    [self addButtonWithText:@"/" x:300 y:210 sel:@selector(division)];
    [self addButtonWithText:@"%" x:230 y:210 sel:@selector(mod)];
    [self addButtonWithText:@"C" x:230 y:130 sel:@selector(c)];
    

}


- (void)plus{
    self.val1 = self.input.text.intValue;
    self.lastOperation = 1;
    self.input.text = @"";
}

- (void)minus{
    self.val1 = self.input.text.intValue;
    self.lastOperation = 2;
    self.input.text = @"";
}

- (void)division{
    self.val1 = self.input.text.intValue;
    self.lastOperation = 3;
    self.input.text = @"";
}

- (void)mult{
    self.val1 = self.input.text.intValue;
    self.lastOperation = 4;
    self.input.text = @"";
}
- (void)mod{
    self.val1 = self.input.text.intValue;
    self.lastOperation = 5;
    self.input.text = @"";
}

- (void)c{

    self.input.text = @"0";
}


-(void)equal{
    int newVal1 = self.input.text.intValue;
    switch (self.lastOperation) {
        case 1:
            self.input.text = [NSString stringWithFormat: @"%i", self.val1 + newVal1];
            break;
        case 2:
            self.input.text = [NSString stringWithFormat: @"%i", self.val1 - newVal1];
            break;
        case 3:
            self.input.text = [NSString stringWithFormat: @"%i", self.val1 / newVal1];
            break;
        case 4:
            self.input.text = [NSString stringWithFormat: @"%i", self.val1 * newVal1];
            break;
        case 5:
            self.input.text = [NSString stringWithFormat: @"%i", self.val1 % newVal1];
            break;
            
        default:
            break;
    }
    
    self.val1 = newVal1;
    
}


-(void) add:(UIButton*)btn{
    NSString* currentText = self.input.text;
 
    if(self.input.text.intValue == 0){
        if([btn.currentTitle compare:@"0"] != -1){
            self.input.text = btn.currentTitle;
        }
    } else {
        self.input.text = [currentText stringByAppendingString:btn.currentTitle];
    }

}

- (void)addButtonWithText:(NSString*)text x:(CGFloat)x y:(CGFloat) y{
    UIButton* l1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    l1.layer.cornerRadius = 10;
    l1.clipsToBounds = YES;
    [l1 setFrame:CGRectMake(x, y, 60, 60)];
    [l1 setTitle:text forState:UIControlStateNormal];
    [l1 setTitleColor:[UIColor  whiteColor] forState:UIControlStateNormal];
    [l1 setBackgroundColor:[UIColor colorWithRed:219/255.0 green:45/255.0 blue:156/255.0 alpha:0.7]];
    [self.view addSubview:l1];
    [l1 addTarget:self action:@selector(add:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)addButtonWithText:(NSString*)text x:(CGFloat)x y:(CGFloat) y sel:(SEL)sel{
    UIButton* l1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    l1.layer.cornerRadius = 10;
    l1.clipsToBounds = YES;
    [l1 setFrame:CGRectMake(x, y, 60, 60)];
    [l1 setTitle:text forState:UIControlStateNormal];
    [l1 setTitleColor:[UIColor  whiteColor] forState:UIControlStateNormal];
    [l1 setBackgroundColor:[UIColor colorWithRed:219/255.0 green:45/255.0 blue:156/255.0 alpha:0.7]];
    [self.view addSubview:l1];
    [l1 addTarget:self action:sel forControlEvents:UIControlEventTouchUpInside];
}



- (void)addButtonWithText:(NSString*)text x:(CGFloat)x y:(CGFloat) y width:(CGFloat)width height:(CGFloat)height sel:(SEL)sel {
    UIButton* l1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    l1.layer.cornerRadius = 10;
    l1.clipsToBounds = YES;
    [l1 setFrame:CGRectMake(x, y, width, height)];
    [l1 setTitle:text forState:UIControlStateNormal];
    [l1 setTitleColor:[UIColor  whiteColor] forState:UIControlStateNormal];
    [l1 setBackgroundColor:[UIColor colorWithRed:219/255.0 green:45/255.0 blue:156/255.0 alpha:0.7]];
    [self.view addSubview:l1];
    [l1 addTarget:self action:sel forControlEvents:UIControlEventTouchUpInside];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
